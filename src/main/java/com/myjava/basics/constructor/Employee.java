package com.myjava.basics.constructor;

public class Employee  {
	private int id;
	private String name;
	private Float salary;
	
public Employee() {
		
	}

	public Employee(int id, String name, Float salary) {
		
		this.id = id;
		this.name = name;
		this.salary = salary;
	}

	@Override
	public String toString() {
		
		return "Employee [id=" + id + ", name=" + name + ", salary=" + salary + "]";
	}
	
	


}
