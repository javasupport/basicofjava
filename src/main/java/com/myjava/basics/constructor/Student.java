package com.myjava.basics.constructor;

public class Student {

	private int studentId;
	private String studentName;
	private float studentAge;
	public Student() {
		System.out.println("I am in Student class");
	}
	
	public int getStudentId() {
		return studentId;
	}

	public void setStudentId(int studentId) {
		this.studentId = studentId;
	}

	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public float getStudentAge() {
		return studentAge;
	}

	public void setStudentAge(float studentAge) {
		this.studentAge = studentAge;
	}

	public void student() {
		System.out.println("I am in student function");
		
	}
	
		

	}


