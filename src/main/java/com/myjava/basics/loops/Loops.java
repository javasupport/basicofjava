package com.myjava.basics.loops;

public class Loops {

	public static void main(String[] args) {
		Loops loops=new Loops();
	    loops.checkForLoop();
	    loops.checkWhileLoop();
	    loops.checkDoWhile();
	    
		

	}

	public void checkForLoop() {
		System.out.print("For loop");
		for (int i = 0; i < 5; i++) {

			System.out.print(i + " ");
		}
		System.out.println();
	}

	public void checkWhileLoop() {
		int i=0;
		System.out.print("while loop");
		while(i<5) {
			System.out.print(i + " ");
			i++;
		}
			
		System.out.println();
	
	}

	public void checkDoWhile() {
		int i=0;
		System.out.print("do while loop");
		do {
			System.out.print(i + " ");
			i++;
		} while (i<5);
		System.out.println();
	}
}
