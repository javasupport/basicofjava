package com.myjava.basics.modifier;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

import com.myjava.basics.myinterface.Add;

public abstract class Calculator  implements Add{

	public void addition(int i, int b) {
		int result= i+b;
		System.out.println("addition is "+result);
		
	}

	/*public void subtraction(int i, int b) {
		int result= i-b;
		System.out.println("addition is "+result);
		
		
	}*/

	// function declaration
	// access modfier return type function name argument then the body
	/*protected int addition(int val1, int val2) {
		int result = val1 + val2;
		return result;
	}

	public int subtraction(int val1, int val2) {
		int result = val1 - val2;
		return result;
	}

	public int multiplication(int val1, int val2) {
		int result = val1 * val2;
		return result;
	}*/

	public int division(int val1, int val2) throws ArithmeticException{
		int result = val1 / val2;
		return result;

	}
	public  void readMyfile() {
		File file = new File("E://file.txt");
	      try {
			FileReader fr = new FileReader(file);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		}
	}

}
