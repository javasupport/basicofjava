package com.myjava.basics.mycollection;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class MyHashMap {

	public void addDataToMap() {
		Map<Integer, String> myMap = new HashMap();

		myMap.put(1, "Pallavi");
		myMap.put(2, "Asraf");
		myMap.put(3, "google");

		System.out.println("My map value is -->" + myMap);

		System.out.println("My  value is -->" + myMap.get(2));

	}

	public void putDataToMap() {
		Map<String, User> myHashMap = new HashMap();
		User obj = new User(30, "nancns@yahoo.com", "dssc", "nxnsssc");
		myHashMap.put("one", obj);
		User obj1 = new User(40, "dss@gmail.com", "sbsj", "dsjc");
		myHashMap.put("two", obj1);

		System.out.println("my hashmap value is " + myHashMap);
		
		Set<String> keySet = myHashMap.keySet();
		for (String key : keySet) {
			System.out.println("value of key "+key);
		}
		
		
		Set<Entry<String, User>> entrySet = myHashMap.entrySet();
		
		for (Entry<String, User> entry : entrySet) {
			User mUser = entry.getValue();
			
			System.out.println(entry.getKey()+"---"+mUser.getUserName());
			System.out.println(entry.getKey()+"---"+mUser.getEmail());
			System.out.println(entry.getKey()+"---"+mUser.getPassword());
			System.out.println("--------------------------");
			
		}
		

	}

	public static void main(String[] args) {
		MyHashMap obj = new MyHashMap();
		obj.addDataToMap();
		obj.putDataToMap();

	}
}
