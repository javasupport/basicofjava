package com.myjava.basics.mycollection;

import java.util.ArrayList;
import java.util.List;

import com.myjava.basics.modifier.Icalci;

public class MyListCollection {
	public void addToMyList() {
		List<User> userList = new ArrayList();
		User obj = new User(12, "pallavik@gmail.com", "asdscs", "pallavikuntumalla");
		userList.add(obj);
		User obj1 = new User(13, "acscs@gmail.com", "dsddd", "cscsz");
		userList.add(obj1);

		showMyListData(userList);
	}

	public void showMyListData(List<User> myUserList) {

		System.out.println("size of my list --->" + myUserList.size());
/*
		for (int i = 0; i < myUserList.size(); i++) {
			User user = myUserList.get(i);
			
			System.out.println(user.getUserId());
			System.out.println(user.getUserName());
			System.out.println(user.getPassword());
			System.out.println(user.getEmail());
			System.out.println("---------------------------------------");

		}*/


		
		for (User user : myUserList) { // for each
			System.out.println(user.getUserId());
			System.out.println(user.getUserName());
			System.out.println(user.getPassword());
			System.out.println(user.getEmail());
			System.out.println("---------------------------------------");
		}
		
	}

	public void getMyList() {
		List<Integer> myList = new ArrayList();

		myList.add(1);
		myList.add(23);
		myList.add(0);
		myList.add(23);

		System.out.println("Mylist value " + myList);

	}

	public static void main(String[] args) {

		MyListCollection myListCollection = new MyListCollection();
		//myListCollection.getMyList();

		myListCollection.addToMyList();

	}

}
