package com.myjava.basics.mycollection;

import java.util.HashSet;
import java.util.Set;

public class MySetCollection {

	
	public void getMySetData() {
		Set mySet=new HashSet();
		mySet.add(1);
		mySet.add(23);
		mySet.add(0);
		mySet.add(23);
		System.out.println("My Set value" +mySet);
		
		
	}
	
	public void addToMySet() {
		Set<User> userSet=new HashSet();
		User obj=new User(10, "asadsd@yahoo.com", "sdc", "dscdvd");
		userSet.add(obj);
		User obj1=new User(20, "sscv@yahoo.com", "sscc", "svdv");
		userSet.add(obj1);
		showMySetData(userSet);
		
		
	}
	
	
	private void showMySetData(Set<User> userSet) {
		System.out.println("size of my Set" +userSet.size());	
		for (User user : userSet) {
			System.out.println(user.getUserId());
			System.out.println(user.getUserName());
			System.out.println(user.getPassword());
			System.out.println(user.getEmail());
			System.out.println("--------------------------");
			
		}
		
	}

	public static void main(String[] args) {
		MySetCollection obj=new MySetCollection();
		obj.getMySetData();
		obj.addToMySet();
		
	}
	
}
