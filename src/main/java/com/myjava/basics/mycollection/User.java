package com.myjava.basics.mycollection;

public class User {
	private int userId;
	private String email, password, userName;

	public User() {
	}

	public User(int userId, String email, String password, String userName) {

		this.userId = userId;
		this.email = email;
		this.password = password;
		this.userName = userName;
	}

	public int getUserId() {
		return userId;
	}

	public String getEmail() {
		return email;
	}

	public String getPassword() {
		return password;
	}

	public String getUserName() {
		return userName;
	}

}
